package com.diksha.calculator

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.diksha.calculator.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var lastNumeric: Boolean = false
    private var isDotPresent: Boolean = false
    private var previousOperator= ""
    private var isNum1Set: Boolean = false

    private var num1: Double = 0.0
    private var num2: Double = 0.0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }

    fun onDigit(view: View) {
        binding.tvInput.append((view as Button).text)
        lastNumeric = true
    }

    fun onOperator(view: View) {
        try {
            if(!isNum1Set) {
                // I am pressing the operator for the  1st time, so we need to set the input as number 1
                num1 = binding.tvInput.text.toString().toDouble()
                previousOperator = (view as Button).text.toString()
                resetInput()
                isNum1Set = true
            } else {
                num2 = binding.tvInput.text.toString().toDouble()
                num1 = evaluate(num1, num2, previousOperator)
                binding.tvResult.text = num1.toString()
                previousOperator = (view as Button).text.toString()
                resetInput()
            }
        } catch(e: NumberFormatException) {
            Toast.makeText(this, "Some error occured!", Toast.LENGTH_SHORT).show()
        }
    }

    fun onDot(view: View) {
        if(lastNumeric && !isDotPresent) {
            binding.tvInput.append(".")
            lastNumeric = false
            isDotPresent = true
        }
    }

    fun onClear(view: View) {
        loadDefaultState()
    }

    fun onEquals(view: View) {
        if(isNum1Set) {
            num2 = binding.tvInput.text.toString().toDouble()
            num1 = evaluate(num1, num2, previousOperator)
            binding.tvResult.text = num1.toString()
            resetInput()
            isNum1Set = false
        } else {
            Toast.makeText(this, "You haven't Selected any operation to be performed", Toast.LENGTH_SHORT).show()
        }
    }

    private fun resetInput() {
        binding.tvInput.text = ""
        lastNumeric = false
        isDotPresent = false
    }

    private fun evaluate(num1: Double, num2: Double, operator: String): Double {
        when(operator) {
            "+" -> return num1 + num2
            "-" -> return num1 - num2
            "/" -> return num1 / num2
            "x" -> return num1 * num2
            else -> return 0.0
        }
    }
    private fun loadDefaultState() {
        resetInput()
        isNum1Set = false
        previousOperator = ""
        num1 = 0.0
        num2 = 0.0
        binding.tvResult.text = ""
    }
}